import 'package:flutter/material.dart';

void main(){
  runApp(const custompaint());
}
// ignore: camel_case_types
class custompaint extends StatelessWidget{
  const custompaint({super.key});

  @override 
  Widget build (BuildContext context){
    return const MaterialApp(
      home: HomePage(),
    );
  }
}
class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Center(child: 
      Container(
        width: 250,height: 250,
        color: Colors.green,
        child: CustomPaint(
          painter: Sky(),
        ),
      ),
    ),

    );
  }
}
class Sky extends CustomPainter{
  @override 
  void paint(Canvas canvas,Size size){
    Paint linepaint = Paint()
    ..color = Colors.red
    ..strokeWidth = 30;

    canvas.drawLine(const Offset(0, 0),
    Offset(0,size.height),
    linepaint,
    );
  }

  @override 
  bool shouldRepaint(covariant CustomPainter oldDelegate) => false;
}