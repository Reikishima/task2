import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:task2/TeksHome.dart';
import 'package:task2/page1.dart';
import 'package:task2/picture.dart';
import 'package:task2/custompaint.dart';
void main() =>
  runApp(const MyApp());
class MyApp extends StatelessWidget {
  const MyApp({super.key});
static const appTitle = "Task 2";

  @override 
  Widget build (BuildContext context){
    return const MaterialApp(title: appTitle, home: MyHomePage(title : appTitle),);
}
}
int _selectedIndex = 0;
class MyHomePage extends StatefulWidget{
  const MyHomePage ({super.key, required this.title});
  @override
  State<MyHomePage> createState() => _MyHomePage();
  final String title;
}
class _MyHomePage extends State<MyHomePage>{
   static final List<Widget> _widgetOption = <Widget>[
   //Text('data'),
     const picture(),
    const TeksHome(),
    //Text('data')
    const custompaint(),
  ];
  
 void _onItemTapped(int index){
      setState((){
        _selectedIndex = index;
      });
      
  }
  @override 
  Widget build (BuildContext context)
  {
    
    return Scaffold(appBar: AppBar(title: const Text('Task 2')),body: Center(child: _widgetOption.elementAt(_selectedIndex),),
     bottomNavigationBar: BottomNavigationBar(items: const <BottomNavigationBarItem>[BottomNavigationBarItem(icon: Icon(Icons.home),label: 'Home'),
    BottomNavigationBarItem(icon: Icon(Icons.picture_in_picture_alt_rounded),label: 'Picture'),
    BottomNavigationBarItem(icon: Icon(Icons.color_lens),label: 'Custom Paint')
    ],
    currentIndex: _selectedIndex,
    selectedItemColor: Colors.amber[800],
    onTap: _onItemTapped,
    ),
    drawer: Drawer(
      // add list view di dalam drawer
      //through the option if isn't enough for vertical
      //space to fit everything.
      child: ListView(
        //remove any padding in listview
        padding: EdgeInsets.zero,
        children: [
          const DrawerHeader(decoration: BoxDecoration(color: Color.fromARGB(255, 8, 210, 15)),
          child: Text('Menu'),
          ),
          ListTile(
            title: const Text('Item 1'),onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: ((context) {
                return const page1();
              }))
              
              );
            },
          ),
          ListTile(
            title: const Text('Item 2'),onTap: () {
              Navigator.pop(context);
            },
          )
        ],
      ),
    ),
    floatingActionButton: FloatingActionButton(onPressed: () {
    },child: const Icon(Icons.add),
    ), 
    );
  }
}

constSky() {
}
class ABC extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final Rect rect = Offset.zero & size;
    const RadialGradient gradient = RadialGradient(
      center: Alignment(0.7, -0.6),
      radius: 0.2,
      colors: <Color>[Color(0xFFFFFF00), Color(0xFF0099FF)],
      stops: <double>[0.4, 1.0],
    );
    canvas.drawRect(
      rect,
      Paint()..shader = gradient.createShader(rect),
    );
  }

  @override
  SemanticsBuilderCallback get semanticsBuilder {
    return (Size size) {
      // Annotate a rectangle containing the picture of the sun
      // with the label "Sun". When text to speech feature is enabled on the
      // device, a user will be able to locate the sun on this picture by
      // touch.
      Rect rect = Offset.zero & size;
      final double width = size.shortestSide * 0.4;
      rect = const Alignment(0.8, -0.9).inscribe(Size(width, width), rect);
      return <CustomPainterSemantics>[
        CustomPainterSemantics(
          rect: rect,
          properties: const SemanticsProperties(
            label: 'Sun',
            textDirection: TextDirection.ltr,
          ),
        ),
      ];
    };
  }

  // Since this Sky painter has no fields, it always paints
  // the same thing and semantics information is the same.
  // Therefore we return false here. If we had fields (set
  // from the constructor) then we would return true if any
  // of them differed from the same fields on the oldDelegate.
  @override
  bool shouldRepaint(ABC oldDelegate) => false;
  @override
  bool shouldRebuildSemantics(ABC oldDelegate) => false;
}
