// ignore: duplicate_ignore
// ignore: file_names
// ignore_for_file: file_names

import 'package:flutter/material.dart';

void main() {
  runApp(const TeksHome());
}
class TeksHome extends StatelessWidget{
  const TeksHome({super.key});

  @override 
  Widget build (BuildContext context){
    return const MaterialApp(
    home:  Home(),
    );
  }
}
class Home extends StatelessWidget{
  const Home({super.key});

  @override 
  Widget build(BuildContext context){
    return Scaffold(
    body: Column(
      children: [
        Expanded(
          child: Container(
            color: Colors.amber,
            height: 100,
            child: const Text('Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. »'),
          ),
        ),
        const Flexible(
          fit: FlexFit.tight,
          child: Image(image: NetworkImage('https://flutter.github.io/assets-for-api-docs/assets/widgets/owl.jpg'),
          ),
        ),
      ],
    ),
    );
  }
}