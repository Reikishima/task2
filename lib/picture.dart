import 'package:flutter/material.dart';

// ignore: camel_case_types
class picture extends StatelessWidget {
  const picture ({Key? key}) :  super(key: key);

  @override 
  Widget build (BuildContext context){
      return Container(
        decoration: BoxDecoration(
          image: const DecorationImage(image: NetworkImage('https://flutter.github.io/assets-for-api-docs/assets/widgets/owl.jpg'),
          fit: BoxFit.cover,
          ),
          border: Border.all(
            color: Colors.blue,
            width: 5,
          ),
          borderRadius: BorderRadius.circular(12),
        ),
        height: 300,width: 300,
        child: const Text(
        '', style: TextStyle(
          fontSize: 15, color: Colors.white,
        ),
        ),
      );
  }
}