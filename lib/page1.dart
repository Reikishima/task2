import 'dart:async';

import 'package:flutter/material.dart';
import 'package:task2/buttonWidget.dart';

void main() => runApp( const page1());

// ignore: camel_case_types
class page1 extends StatelessWidget{
  const page1({super.key});

  @override 
  Widget build (BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: const DemoApp(),
      theme: ThemeData.dark(),
    );
  }
}


class DemoApp extends StatefulWidget{
  const DemoApp({super.key});

  @override 
  // ignore: library_private_types_in_public_api
  _DemoAppState createState() => _DemoAppState();
}

class _DemoAppState extends State<DemoApp>{
  static const maxSeconds = 60;
  int seconds  = maxSeconds;
  Timer? timer;

  void resetTimer() => setState(() =>  seconds = maxSeconds);

  void startTimer({bool reset = true}){
      if (reset){
        resetTimer();
      }


    timer = Timer.periodic(const Duration(milliseconds: 59), (_){
      if(seconds > 0){

          setState(() => seconds--);
      }else{
        stopTimer(reset: false);
      }
    });
  }

  void stopTimer({bool reset = true}){
    if(reset){
      resetTimer();
    }
    setState(() => timer?.cancel());
  }

  @override 
  Widget build (BuildContext context) => Scaffold(
    body: Center(child: Column(mainAxisAlignment: MainAxisAlignment.center,children: [
        buildTimer(),
        const SizedBox(height: 80,),
        buildButtons(),
    ],)
    ),  
    
    );
    Widget buildButtons(){
      final isRunning = timer == null ? false: timer!.isActive;
      final isCompleted = seconds == maxSeconds || seconds == 0;
      return isRunning || !isCompleted?
      Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children :[
      ButtonWidget(text: isRunning ? 'Pause': 'Resume',onClicked:() {
        if (isRunning){

       stopTimer(reset: false); 
        }else{
        startTimer(reset: false);
        }
      },
      ),
      const SizedBox(width: 12,),
      ButtonWidget(text: 'Cancel', onClicked:stopTimer,
      ),
      ],
      )
      
      : ButtonWidget(
        text: 'Start Timer!',
        color: Colors.black,
        backgroundColor: Colors.white,
        onClicked: (){
          startTimer();
        },
      );
    }

    Widget buildTimer() => SizedBox(
      width: 200,
      height: 200,
      child: Stack(
        children: [
          CircularProgressIndicator(
            value: seconds / maxSeconds,
          ),
          Center(child: buildTime(),)
        ],
    ),
    );



    Widget buildTime(){
      return Text('$seconds',
      style: const TextStyle(
        fontWeight: FontWeight.bold,
        color: Colors.white,
        fontSize: 80,
        
      ),
      );
    }
}
